"""Conway's game of life."""
import numpy as np
from scipy.signal import convolve2d


def step(x):
    """Step one generation of Conway's game of life.

    Arguments:
    x -- 2D boolean array of living cells.

    """
    kernel = np.ones((3, 3), int)
    kernel[1, 1] = 0
    neighbors = convolve2d(x, kernel, mode='same')
    reproduce = neighbors == 3
    keep_alive = np.all(np.stack((neighbors == 2, x)), axis=0)
    return np.any(np.stack((reproduce, keep_alive)), axis=0).astype(x.dtype)
