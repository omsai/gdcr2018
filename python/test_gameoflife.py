from itertools import combinations

import numpy as np
import pytest

from gameoflife import step


def simulate(neighbors, center=True, grid=5):
    """Generate all possible boolean arrays for a number of neighbors."""
    mid = int(np.floor(grid / 2))
    # Neighbors are 8 positions.
    for indices in combinations(range(8), neighbors):
        # Indices represent the neighbors.
        #
        # We want:
        # 0 1 2
        # 3   5
        # 6 7 8
        indices = np.array(indices, int)
        indices[indices >= 4] += 1
        # Create 3x3 grid with our neighbors.
        mask = np.zeros(9, bool)
        mask[indices] = True
        # Put the 3x3 grid into a 5x5 grid so that our simulation
        # gives the same result for bounded and unbounded game of life
        # problems.
        arr = np.zeros((grid, grid), int)
        arr[mid-1:mid+2, mid-1:mid+2] = mask.reshape((3, 3))
        arr[mid, mid] = center
        assert np.sum(arr[mid-1:mid+2, mid-1:mid+2]) == neighbors + center
        yield arr


oscillator = [
    np.array([[0, 0, 0, 0, 0, 0],
              [0, 0, 0, 1, 0, 0],
              [0, 1, 0, 0, 1, 0],
              [0, 1, 0, 0, 1, 0],
              [0, 0, 1, 0, 0, 0],
              [0, 0, 0, 0, 0, 0]], int),

    np.array([[0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0],
              [0, 0, 1, 1, 1, 0],
              [0, 1, 1, 1, 0, 0],
              [0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0]], int),
]

glider = [
    np.array([[0, 0, 0, 0, 0],
              [0, 0, 1, 0, 0],
              [1, 0, 1, 0, 0],
              [0, 1, 1, 0, 0],
              [0, 0, 0, 0, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 1, 0, 0, 0],
              [0, 0, 1, 1, 0],
              [0, 1, 1, 0, 0],
              [0, 0, 0, 0, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 0, 1, 0],
              [0, 1, 1, 1, 0],
              [0, 0, 0, 0, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 1, 0, 1, 0],
              [0, 0, 1, 1, 0],
              [0, 0, 1, 0, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 1, 0],
              [0, 1, 0, 1, 0],
              [0, 0, 1, 1, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 0, 1, 1],
              [0, 0, 1, 1, 0]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 1, 0],
              [0, 0, 0, 0, 1],
              [0, 0, 1, 1, 1]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 1, 0, 1],
              [0, 0, 0, 1, 1]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 1, 1]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 1, 1],
              [0, 0, 0, 1, 1]], int),

    np.array([[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 1, 1],
              [0, 0, 0, 1, 1]], int),
]


@pytest.mark.parametrize("x",
                         list(simulate(0)) +
                         list(simulate(1)))
def test_underpopulation_death(x):
    y = step(x)
    mid = int(np.floor(y.shape[0] / 2))
    assert y[mid, mid] == 0


@pytest.mark.parametrize("x",
                         list(simulate(4)) +
                         list(simulate(5)) +
                         list(simulate(6)) +
                         list(simulate(7)) +
                         list(simulate(8)))
def test_overpopulation_death(x):
    y = step(x)
    mid = int(np.floor(y.shape[0] / 2))
    assert y[mid, mid] == 0


@pytest.mark.parametrize("x",
                         list(simulate(2)) +
                         list(simulate(3)))
def test_sufficient_population_persistence(x):
    y = step(x)
    mid = int(np.floor(y.shape[0] / 2))
    assert y[mid, mid] == 1


@pytest.mark.parametrize("x",
                         list(simulate(3, center=False)))
def test_reproduction(x):
    y = step(x)
    mid = int(np.floor(y.shape[0] / 2))
    assert y[mid, mid] == 1


@pytest.mark.parametrize("input_,expected", zip(oscillator,
                                                oscillator[::-1]))
def test_oscillator(input_, expected):
    assert np.array_equal(step(input_), expected)


@pytest.mark.parametrize("input_,expected", zip(glider[:-1],
                                                glider[1:]))
def test_glider(input_, expected):
    assert np.array_equal(step(input_), expected)
